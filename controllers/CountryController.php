<?php

namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\Country;

class CountryController extends Controller
{
    public function actionIndex()
    {

        $models = Country::find()->where(['=>','population','B'])->createCommand()->rawSql;
        var_dump($models);
        exit();

        foreach ($models as $model){
            echo $model->code . "/" . $model->name . "/" . $model->population . "</br>";
        }
        var_dump($models);
        die();

        $query = Country::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $countries = $query->orderBy('name')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'countries' => $countries,
            'pagination' => $pagination,
        ]);
    }
}