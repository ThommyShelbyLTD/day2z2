<?php

namespace app\controllers;

use app\models\MailForm;
use Yii;
use yii\debug\models\search\Mail;
use yii\web\Controller;

class MyController extends Controller
{
    public function actionIndex($fio = 'Укажите ФИО:')
    {
        return $fio;
    }


    public function actionAbout()
    {
        return $this->render('about',[
            'firstname'=>'Крутыш'
        ] );
    }

    public function actionDragon()
    {
        return $this->render('dragon',[
            'skills'=>'Dragonski'
        ]);
    }

    public function actionBelive()
    {
        return $this->render('belive',[
            'img'=>'НямНямНям'
        ]);
    }

    public function actionMail()
    {
        $model = new MailForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }

            return $this->render('mail',[
                'model'=>$model

                ]);
    }



}

