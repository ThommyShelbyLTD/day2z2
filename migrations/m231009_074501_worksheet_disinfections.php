<?php

use yii\db\Migration;

/**
 * Class m231009_074501_worksheet_disinfections
 */
class m231009_074501_worksheet_disinfections extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worksheet_disinfections', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'type' => $this->char(255)->notNull(),
            'organization_name' => $this->char(255),
            'adress' => $this->char(255),
            'boxs' => $this->text()->notNull(),
            'area_all' => $this->double()->notNull(),
            'unit' =>$this->char(50)->notNull()->defaultValue('m2'),
            'method_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'medicine_name:' => $this->char(255)->notNull(),
            'dosage' => $this->char(50)->notNull(),
            'time' => $this->integer(),
            'date_event' => $this->integer()->notNull(),
            'report_date' => $this->integer()->notNull(),
            'create_user_id' => $this->integer()->notNull(),
            'start_processing' => $this->char(255),
            'end_processing' => $this->char(255),
            'exposition_time'=>$this->time(),
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worksheet_disinfections');


    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231009_074501_worksheet_disinfections cannot be reverted.\n";

        return false;
    }
    */
}
